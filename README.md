# UML期末考试
-------------
#### 项目介绍
UML期末考试试题，七个人一组，每组选出一个小组长，从给出的题目中选择一个。期末上交程序和UML文档。每个步骤都有期末考试分数，都体现在此网站上。根据每组的项目完成情况，以及做的过程，给出分数，每组都有一个20分的附加题，根据完成情况，可得到0-20分的额外分数。

#### 考试步骤

1. 自由分组（一周，1分）：7人一组，选出小组长
2. 申请账号（一周，2分）：每人以自己名字的拼音全拼（如果重名，可加后缀）作为用户名，在码云https://gitee.com申请一个账号，每个组长建立一个仓库，这个仓库用来存放每组的程序和文档。（总结：每组一个仓库，每人一个账号）
3. 选题（一周,2分）：从下面列表中选择一个题目（十一放假前可自己选择题目，放假后必须从下表中选择）；组长关注此项目，把选题和相关信息填入GroupList_jike.md（计科班）或GroupList_wulianwang.md（物联网班）。（总结：每组一个题目）
4. 上传代码（二周，1分）：每组把各自程序源码的初始版本上传到码云网站的各组仓库中。
5. 修改代码（直到考试结束，5分）：小组成员为代码做贡献，直至完成程序（包括程序代码、说明文档、各种UML模型）
6. 课堂演讲（直到考试结束，4分）：在程序编写过程中，进行课堂讲演，上机课进行完善，同时对每个组的进度进行检查（可以包括以下内容：本阶段完成的工作，工作中碰到的技术，技术原理是什么，碰到的问题是什么，下一个阶段计划怎么解决等）
7. 期末演讲（期末考试前，50分）：提交课程设计报告和程序及文档到仓库中；进行演讲，对整个项目进行讲解。


#### 候选列表

1. Tensorflow object detection API 物体检测手机APP （源码：https://github.com/tensorflow/tensorflow/tree/master/tensorflow/examples/android）
附加题：在上面源代码的基础上，打造属于自己的物体检测模型（https://www.bilibili.com/video/av21539370/?p=1）

2. 利用dlib进行人脸识别（https://github.com/ageitgey/face_recognition）
附加题：在上面源代码的基础上，实现人脸识别签到
3. 利用yolov3进行物体检测（https://github.com/AlexeyAB/darknet）
附加题：在上面源代码的基础上，打造属于自己的物体检测模型
4. 利用facenet进行人脸识别（https://github.com/davidsandberg/facenet）
附加题：在上面源代码的基础上，实现人脸识别签到（https://github.com/008karan/Face-recognition）
5. 用ssd模型物体检测（https://github.com/balancap/SSD-Tensorflow）
附加题：在上面源代码的基础上，打造属于自己的物体检测模型
6. 用yolov3模型物体检测（https://github.com/qqwweee/keras-yolo3）
附加题：在上面源代码的基础上，打造属于自己的物体检测模型
7. 利用手机摄像头识别出手机号码（https://github.com/chen1311j/PhoneNumberOCRApp）
附加题：在上面源代码基础上，打造一款通过摄像头识别出手机号码，并能辅助打出电话的app
8. 利用CNN卷积神经网络对中文新闻进行分类（https://github.com/gaussic/text-classification-cnn-rnn）
附加题：在上面源代码基础上，利用自己的数据打造属于自己的物体分类模型

#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request

[请点击查看具体步骤](https://gitee.com/WeiKeAI/UML/blob/master/%E5%A6%82%E4%BD%95%E4%BF%AE%E6%94%B9%E8%BF%99%E9%87%8C%E7%9A%84%E4%BF%A1%E6%81%AF.md)

#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)